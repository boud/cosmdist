#!/bin/bash
# The value given to standard input for this check is only of minor
# importance, but it should be a reasonable value for a redshift.
echo 1.0 | ./cosmdist --check
