#!/bin/bash

# The EdS distance at z=3 should be 299792458/100000 Mpc exactly for
# H_0 = 100 km/s/Mpc. Check this at double precision level to within
# tol.

LANG=C
LC_ALL=C

# cosmdist EdS gives:
z3_EdS_dist=$(echo 3 |./cosmdist -l 0 -m 1 -r 0 -h 100 -o 15)
# expected exact value:
z3_EdS_dist0=2997.92458

tol=1e-15

printf "EdS distance at z=3: calculated = ${z3_EdS_dist}  expected = ${z3_EdS_dist0}\n"
pass_value=$(printf "${z3_EdS_dist} ${z3_EdS_dist0}" | \
                 awk -v t=${tol} '{print (-t < (($1-$2)/$2) && (($1-$2)/$2) < t) ? 0 : 1}')
printf "pass_value = %s\n" ${pass_value}

exit ${pass_value}
